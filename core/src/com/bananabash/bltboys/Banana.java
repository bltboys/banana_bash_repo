package com.bananabash.bltboys;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g3d.particles.influencers.ColorInfluencer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import java.util.Random;

/**
 * Created by liamgensel on 3/8/16.
 */
public class Banana {

    private Texture _bananaTexture;
    private Rectangle _bananaRectangle;
    private int _bananaSpeedVariation = 0;
    private Score _score;
    private ParticleEffect _partParticleEffect;


    public Banana(Score score, Texture texture) {
        _score = score;
        _bananaTexture = texture;
        _bananaRectangle = new Rectangle(0, 0, 420/2, 241/2);

        _partParticleEffect = new ParticleEffect();
        _partParticleEffect.load(Gdx.files.internal("particle.p"), Gdx.files.internal(""));

        spawnBanana();
    }

    public void renderBanana(SpriteBatch batch, Vector3 touchPosition){
        updateBanana(touchPosition);
        batch.draw(getTexture(), _bananaRectangle.x, _bananaRectangle.y, _bananaRectangle.width, _bananaRectangle.height);
        _partParticleEffect.draw(batch, Gdx.graphics.getDeltaTime());
    }

    private void updateBanana(Vector3 touchPosition){
        _bananaRectangle.y -= (400 + _bananaSpeedVariation) * Gdx.graphics.getDeltaTime();
        if(_bananaRectangle.y<-_bananaRectangle.getHeight())spawnBanana();
        if(touchPosition != null){
            if(_bananaRectangle.contains(touchPosition.x, touchPosition.y))blowUpBanana();
        }
    }

    private void spawnBanana(){
        Random random = new Random();
        _bananaRectangle.setY(1280 + _bananaRectangle.height);
        _bananaRectangle.setX(random.nextInt((int) (720 - _bananaRectangle.getWidth())));
        _bananaSpeedVariation = random.nextInt(200);

    }

    private void blowUpBanana(){
        _score.incrementScore();
        _partParticleEffect.setPosition(_bananaRectangle.getX() + _bananaRectangle.getWidth()/2, _bananaRectangle.getY() + _bananaRectangle.getHeight()/2);
        _partParticleEffect.start();
        spawnBanana();
    }

    private Texture getTexture() {
        return this._bananaTexture;
    }

}
