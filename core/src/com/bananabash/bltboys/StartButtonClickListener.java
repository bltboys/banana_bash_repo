package com.bananabash.bltboys;

import com.bananabash.bltboys.Sceens.GameScreen;

/**
 * Created by leshan on 3/15/16.
 */
public class StartButtonClickListener implements OnClickListener{

    Banana_Bash _banana_bash;

    public StartButtonClickListener(Banana_Bash banana) {

        _banana_bash = banana;

    }

    @Override
    public void onClick(Button button) {

        _banana_bash.setScreen(new GameScreen(_banana_bash));
        _banana_bash.dispose();

    }
}
