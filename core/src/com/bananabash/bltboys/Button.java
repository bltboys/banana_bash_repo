package com.bananabash.bltboys;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

import java.awt.*;

/**
 * Created by liamgensel on 3/12/16.
 */
public class Button {
    private Texture _texture;
    private Rectangle _buttonRectangle;
    private OnClickListener _onOnClickListener;

    public Button(Texture texture, int x, int y, int width, int height) {
        _texture = texture;
        _buttonRectangle = new Rectangle();
        _buttonRectangle.x = x;
        _buttonRectangle.y = y;
        _buttonRectangle.width = width;
        _buttonRectangle.height = height;

    }

    public void render(SpriteBatch batch, OrthographicCamera camera){
        update(camera);
        batch.draw(_texture, _buttonRectangle.x, _buttonRectangle.y, _buttonRectangle.width, _buttonRectangle.height);
    }

    private void update(OrthographicCamera camera){

        Vector3 touchPosition = null;
        if (Gdx.input.justTouched()) {
            touchPosition = new Vector3();
            touchPosition.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPosition);
            if(touchPosition != null){
                if(_buttonRectangle.contains(touchPosition.x, touchPosition.y)){
                    _onOnClickListener.onClick(this);
                }
            }
        }

    }

    public void setOnclickListener(OnClickListener clickListener){
        _onOnClickListener = clickListener;
    }

    public Texture getTexture() {
        return _texture;
    }

    public Rectangle getButtonRectangle() {
        return _buttonRectangle;
    }

}
