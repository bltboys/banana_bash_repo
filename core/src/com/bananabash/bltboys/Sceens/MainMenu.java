package com.bananabash.bltboys.Sceens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.bananabash.bltboys.Banana_Bash;
import com.bananabash.bltboys.Button;
import com.bananabash.bltboys.Score;
import com.bananabash.bltboys.StartButtonClickListener;

/**
 * Created by liamgensel on 3/12/16.
 */
public class MainMenu implements Screen {
    private SpriteBatch batch;
    private OrthographicCamera camera;
    private BitmapFont font;
    private Banana_Bash _banana;
    private Texture _logo;
    private ParticleEffect _effect;
    private Texture _startButtonTexture;
    private Button _startButton;


    public MainMenu(Banana_Bash banana_bash) {
        _banana = banana_bash;
    }

    @Override
    public void show() {
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        _logo = new Texture(Gdx.files.internal("mainMenuLogo.png"));
        //start button
        _startButtonTexture = new Texture(Gdx.files.internal("start.png"));
        _startButton = new Button(_startButtonTexture, (720 - _startButtonTexture.getWidth()) / 2, 580, _startButtonTexture.getWidth(), _startButtonTexture.getHeight());

        //Setting an OnclickListener on the button
        _startButton.setOnclickListener(new StartButtonClickListener(_banana));

        //falling bananas
        _effect = new ParticleEffect();
        _effect.load(Gdx.files.internal("bananaEffect.p"), Gdx.files.internal(""));
        _effect.setPosition(0, 1280);
        //font
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 24;
        parameter.color = com.badlogic.gdx.graphics.Color.WHITE;
        font = generator.generateFont(parameter);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        batch.setProjectionMatrix(camera.combined);

        batch.begin();
        _effect.draw(batch, delta);
        batch.draw(_logo, 0, 980);

        //Button class handles the rendering
        _startButton.render(batch, camera);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {
        camera.setToOrtho(false, 720, 1280);
        camera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
