package com.bananabash.bltboys.Sceens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.bananabash.bltboys.Banana;
import com.bananabash.bltboys.Banana_Bash;
import com.bananabash.bltboys.Score;

/**
 * Created by liamgensel on 3/12/16.
 */
public class GameScreen implements Screen {

    private SpriteBatch batch;
    private OrthographicCamera camera;

    private Array<Banana> bananas = new Array<Banana>();
    private final int NUMBER_OF_BANANAS = 4;
    private Score _score;
    private BitmapFont font;
    private Banana_Bash _banana;


    public GameScreen(Banana_Bash banana_bash) {
        _banana = banana_bash;
    }


    @Override
    public void show() {
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        _score = new Score();

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 128;
        parameter.color = com.badlogic.gdx.graphics.Color.DARK_GRAY;
        font = generator.generateFont(parameter);

        Texture bananaTexture = new Texture(Gdx.files.internal("banana.png"));
        for (int i = 0; i < NUMBER_OF_BANANAS; i++) {
            bananas.add(new Banana(_score, bananaTexture));
        }

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        batch.setProjectionMatrix(camera.combined);

        Vector3 touchPosition = null;

        if (Gdx.input.justTouched()) {
            touchPosition = new Vector3();
            touchPosition.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPosition);
        }

        batch.begin();
        font.draw(batch, Integer.toString(_score.getScore()), 720f / 2f, 1280f / 2f);
        for (Banana banana : bananas) {
            banana.renderBanana(batch, touchPosition);
        }
        batch.end();

    }

    @Override
    public void resize(int width, int height) {
        camera.setToOrtho(false, 720, 1280);
        camera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
