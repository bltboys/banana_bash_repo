package com.bananabash.bltboys;

/**
 * Created by leshan on 3/15/16.
 */
public interface OnClickListener {

    public void onClick(Button button);

}
